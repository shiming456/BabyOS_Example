# BabyOS_Example

【STM32F107 使用STM32标准库函数】

```shell
git clone https://gitee.com/notrynohigh/BabyOS_Example.git
git submodule init
git submodule update
```



------

## example/template/   

### 实验内容：

①基于MCU内部FLASH进行KV存储。 

②通过shell指令进行交互：

```shell
nr@bos:bos -v
Version:7.1.0

nr@bos:kv bos hello
bos hello 5

nr@bos:kv bos
hello
```

## example/ds18b20/   

### 实验内容：

①使用DS18B20温度传感器

```shell
nr@bos:temp -s
nr@bos:temp
r:2625

```



## example/oled/  

### 实验内容：

①使用OLED屏，通过指令显示字符串

[show] [x] [y] [string]

```shell
nr@bos:show 1 2 hello
nr@bos:show 10 10 hello

```



## example/spiflash/  

### 实验内容：

①spiflash基于KV软件模块存储启动设备的次数



## example/24c02/  

### 实验内容：

①at24lc02的0地址存储启动设备的次数



## example/adchub/  

### 实验内容：

①MCU两路ADC结合adchub模块使用

②adchub作为adc值得中转站，将喂进来得数据进行滤波后再调用指定的回调函数。



## example/lis3dh/  

### 实验内容：

①三轴加速度传感器fifo模式，获取加速度值。读取x y z加速度单位mg



## example/button/  

### 实验内容：

①按键使用例子，4个按键，分别注册不同的事件：

   1：单击、双击事件

   2：短按

   3：长按

   4：超长按



## example/sd_fatfs/  

### 实验内容：

①使用sd卡+fatfs，用文件记录开机的次数



## example/spiflash_fatfs/  

### 实验内容：

①使用spiflash+fatfs，用文件记录开机的次数



## example/spi_tft/  

### 实验内容：

①使用SPI接口的TFT屏，控制芯片是ILI9341



## example/ssd1289_tft/  

### 实验内容：

①使用TFT屏，控制芯片是ssd1289



## example/esp12f/  

### 实验内容：

①esp12f wifi模块，烧录MQTT固件

②连接MQTT代理  babyos.cn   端口 1883 用户名 babyos  密码 babyos



## example/error/  

### 实验内容：

①错误管理例子



## example/menu/  

### 实验内容：

①菜单软件模块的使用

②创建了2级菜单，通过4个按键进行操作



## example/param/  

### 实验内容：

①增加对变量TestTick的查询和设置功能

②通过 param TestTick查询变量值

③通过 param TestTick 0 设置变量值为0



## example/protocol/  

### 实验内容：

①增加私有协议的使用例子



## example/pwm/  

### 实验内容：

①使用软件PWM驱动LED，显示不同亮度



## example/timer/  

### 实验内容：

①软件定时器例子



## example/xm128/  

### 实验内容：

①xmodem128传输文件例子



## example/ymodem/  

### 实验内容：

①ymodem传输文件例子

## example/coremark/  

### 实验内容：

①STM32F107跑分

```C
    /   )          /           /    )   /    \
---/__ /-----__---/__---------/----/----\-----
  /    )   /   ) /   ) /   / /    /      \    
_/____/___(___(_(___/_(___/_(____/___(____/___
                         /                    
                     (_ /                     
HW:21.12.12 FW:7.4.8 COMPILE:Aug 15 2022-22:22:11
device number:1
I:No device is registered
I:No device is registered
---------------------------------
--------COREMARK & BabyOS--------
---------------------------------
Please wait .....
2K performance run parameters for coremark.
CoreMark Size    : 666
Total ticks      : 90381
Total time (secs): 90.381000
Iterations/Sec   : 132.771268
Iterations       : 12000
Compiler version : ARMCC V5.06
Compiler flags   : -O3
Memory location  : STATIC
seedcrc          : 0xe9f5
[0]crclist       : 0xe714
[0]crcmatrix     : 0x1fd7
[0]crcstate      : 0x8e3a
[0]crcfinal      : 0xd340
Correct operation validated. See README.md for run and reporting rules.
CoreMark 1.0 : 132.771268 / ARMCC V5.06 -O3 / STATIC
```



## example/state/  

### 实验内容：

①使用状态机功能模块

系统由3个状态：离线、在线和配网

状态附加1个事件表：配网事件

离线：定时ping百度

在线：定时发布MQTT消息

配网：建立AP，接收配网信息

```c
HW:21.12.12 FW:7.4.10 COMPILE:Oct 23 2022-01:17:43
device number:2
I:state 0 --> offline(0) 
I:state 0 --> net_cfg(2) 

tcp:s:notrynohigh p:11223344

ssid:notrynohigh
passwd:11223344
I:state 2 --> offline(0) 
I:state 0 --> Online(1) 
result 2 
error 0 
I:state 1 --> offline(0) 
I:state 0 --> Online(1) 
result 1 

```





BabyOS教程更新会在公众号推送：

![](https://gitee.com/notrynohigh/BabyOS_Example/raw/master/doc/QRcode.jpg)

