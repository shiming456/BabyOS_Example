/**
 ******************************************************************************
 * @file    Project/STM32F10x_StdPeriph_Template/main.c
 * @author  MCD Application Team
 * @version V3.3.0
 * @date    04/16/2010
 * @brief   Main program body
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "b_os.h"
#include "board.h"

/** @addtogroup Template_Project
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/********************************************************************************/

void BtnEventHandler(uint32_t dev_no, uint8_t sub_id, uint16_t event, uint8_t param)
{
    b_log("dev:%d id:%d event:%x param:%d\n", dev_no, sub_id, event, param);
}

/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main()
{
    BoardInit();

    SysTick_Config(SystemCoreClock / TICK_FRQ_HZ);
    NVIC_SetPriority(SysTick_IRQn, 0x0);

    bInit();

    // PE8 PE9 PE10 PE11测试矩阵按键
    bHalGpioConfig(B_HAL_GPIOE, B_HAL_PIN8, B_HAL_GPIO_INPUT, B_HAL_GPIO_PULLUP);
    bHalGpioConfig(B_HAL_GPIOE, B_HAL_PIN9, B_HAL_GPIO_INPUT, B_HAL_GPIO_PULLUP);
    bHalGpioConfig(B_HAL_GPIOE, B_HAL_PIN10, B_HAL_GPIO_INPUT, B_HAL_GPIO_PULLUP);
    bHalGpioConfig(B_HAL_GPIOE, B_HAL_PIN11, B_HAL_GPIO_INPUT, B_HAL_GPIO_PULLUP);

    bBUTTON_ADD_KEY(bKEY1, BTN_EVENT_CLICK | BTN_EVENT_LONG, BtnEventHandler);
    bBUTTON_ADD_KEY(bKEY2, BTN_EVENT_CLICK | BTN_EVENT_LONG, BtnEventHandler);
    bBUTTON_ADD_KEY(bKEY3, BTN_EVENT_CLICK | BTN_EVENT_LONG, BtnEventHandler);
    bBUTTON_ADD_KEY(bKEY4, BTN_EVENT_CLICK | BTN_EVENT_LONG, BtnEventHandler);
    bBUTTON_ADD_MATRIXKEYS(bMATRIXKEYS, BTN_EVENT_CLICK | BTN_EVENT_LONG, BtnEventHandler);
    while (1)
    {
        bExec();
    }
}

void SysTick_Handler()
{
    bHalIncSysTick();
}

void USART1_IRQHandler()
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        USART_ReceiveData(USART1);
    }
}

void ADC1_2_IRQHandler()
{
    if (ADC_GetITStatus(ADC1, ADC_IT_JEOC) == SET)
    {
        ADC_ClearITPendingBit(ADC1, ADC_IT_JEOC);
    }
}

void UART4_IRQHandler()
{
    if (USART_GetITStatus(UART4, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(UART4, USART_IT_RXNE);
        USART_ReceiveData(UART4);
    }
}

#ifdef USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
