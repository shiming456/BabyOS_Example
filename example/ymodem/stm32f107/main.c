/**
 ******************************************************************************
 * @file    Project/STM32F10x_StdPeriph_Template/main.c
 * @author  MCD Application Team
 * @version V3.3.0
 * @date    04/16/2010
 * @brief   Main program body
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "b_os.h"
#include "board.h"

/** @addtogroup Template_Project
 * @{
 */

uint8_t FileBuf[1024];
uint16_t FileLen = 0;

void YModemCallback(uint8_t t, uint8_t *pbuf, uint16_t len)
{
    if(pbuf != NULL && (t == YMODEM_FILEDATA))
    {
        memcpy(&FileBuf[FileLen], pbuf, len);
        FileLen += len;
    }
}

void YmodemSend(uint8_t cmd)
{
    bHalUartSend(HAL_LOG_UART, &cmd, 1);
}

int UartIdleCallback(uint8_t *pbuf, uint16_t len, void *arg)
{
    bYmodemParse(pbuf, len);
    return 0;
}

bUTIL_UART_INSTANCE(YmodemRec, 1128, 50, UartIdleCallback, NULL);

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/********************************************************************************/
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main()
{
    BoardInit();

    SysTick_Config(SystemCoreClock / TICK_FRQ_HZ);
    NVIC_SetPriority(SysTick_IRQn, 0x0);

    bInit();
    bYmodemInit(YModemCallback, YmodemSend);
    bYmodemStart(); 
    while (1)
    {
        bExec();
    }
}

void SysTick_Handler()
{
    bHalIncSysTick();
}

void USART1_IRQHandler()
{
    uint8_t uart_dat = 0;
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        uart_dat = USART_ReceiveData(USART1);
        bUtilUartRxHandler(&YmodemRec, uart_dat);
    }
}

#ifdef USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
