/**
 ******************************************************************************
 * @file    Project/STM32F10x_StdPeriph_Template/main.c
 * @author  MCD Application Team
 * @version V3.3.0
 * @date    04/16/2010
 * @brief   Main program body
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "b_os.h"
#include "board.h"

/** @addtogroup Template_Project
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

int WifiFd = -1;

bWifiApInfo_t ApInfo = {
    .ssid       = "notrynohigh",
    .passwd     = "11223344",
    .encryption = 2,
};

bMqttConnInfo_t MqttInfo = {
    .broker    = "babyos.cn",
    .port      = 1883,
    .device_id = "esp12f",
    .user      = "babyos",
    .passwd    = "babyos",
};

bMqttTopicInfo_t TopicInfo = {
    .topic = "baby",
    .qos   = 1,
};

bMqttTopicData_t TopicData = {
    .topic.topic = "os",
    .topic.qos   = 1,
    .pstr        = "hello babyos",
};

static uint8_t TestFlag = 0;

void WiFiTcpMqttTest()
{
    bDeviceMsg_t msg;
    if (TestFlag == 0)
    {
        //将WIFI模块配置为STA模式
        bCtl(WifiFd, bCMD_WIFI_MODE_STA, NULL);
        TestFlag++;
    }
    else if (TestFlag % 2)
    {
        //获取配置结果
        bDeviceReadMessage(bESP12F, &msg);
        bEsp12fPrivate_t *p = (bEsp12fPrivate_t *)msg._p;
        if (p->result == ESP12F_CMD_RESULT_OK)
        {
            TestFlag += 1;
        }
        else if (p->result == ESP12F_CMD_RESULT_ERR)
        {
            TestFlag--;
            b_log("err %d ...\n", TestFlag);
        }
    }
    else if (TestFlag == 2)
    {
        // 连接路由
        bCtl(WifiFd, bCMD_WIFI_JOIN_AP, &ApInfo);
        TestFlag++;
    }
    else if (TestFlag == 4)
    {
        // 连接MQTT代理
        bCtl(WifiFd, bCMD_WIFI_MQTT_CONN, &MqttInfo);
        TestFlag++;
    }
    else if (TestFlag == 6)
    {
        // 订阅主题 baby
        bCtl(WifiFd, bCMD_WIFI_MQTT_SUB, &TopicInfo);
        TestFlag++;
    }
}


/********************************************************************************/
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main()
{
    bWiFiData_t RecData;
    BoardInit();

    SysTick_Config(SystemCoreClock / TICK_FRQ_HZ);
    NVIC_SetPriority(SysTick_IRQn, 0x0);

    bInit();

    WifiFd = bOpen(bESP12F, BCORE_FLAG_RW);
    while (1)
    {
        BOS_PERIODIC_TASK(WiFiTcpMqttTest, 500);
        //读取数据，来自TCP或者MQTT订阅主题
        if (bRead(WifiFd, (uint8_t *)&RecData, sizeof(bWiFiData_t)) > 0)
        {
            if (RecData.mqtt.pstr != NULL)
            {
                //打印主题和收到的消息
                b_log("%s, %s\n", RecData.mqtt.topic.topic, RecData.mqtt.pstr);
            }
            if (RecData.tcp.pstr != NULL)
            {
                b_log("tcp:%s\n", RecData.tcp.pstr);
            }
            //释放空间
            WIFI_DATA_USE_END(RecData);
        }
        bExec();
    }
}

void SysTick_Handler()
{
    bHalIncSysTick();
}

void USART1_IRQHandler()
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        USART_ReceiveData(USART1);
    }
}

void UART4_IRQHandler()
{
    uint8_t dat = 0;
    if (USART_GetITStatus(UART4, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(UART4, USART_IT_RXNE);
        dat = USART_ReceiveData(UART4);
        bUtilUartRxHandler2(B_HAL_UART_4, dat);
    }   
}

#ifdef USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
