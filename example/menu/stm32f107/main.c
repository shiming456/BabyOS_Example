/**
 ******************************************************************************
 * @file    Project/STM32F10x_StdPeriph_Template/main.c
 * @author  MCD Application Team
 * @version V3.3.0
 * @date    04/16/2010
 * @brief   Main program body
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>

#include "b_os.h"
#include "board.h"
#include "menu.h"
/** @addtogroup Template_Project
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

static void _CmdShowHandler(char argc, char *argv)
{
    int x = 0, y = 0;
    if (argc == 4)
    {
        x = atoi(&argv[argv[1]]);
        y = atoi(&argv[argv[2]]);
        UG_PutString(x, y, &argv[argv[3]]);
    }
}
bSHELL_REG_INSTANCE("show", _CmdShowHandler);

void BtnEventHandler(uint32_t dev_no, uint8_t sub_id, uint16_t event, uint8_t param)
{
    if(dev_no == bKEY_UP)
    {
        bMenuAction(MENU_UP);
    }
    else if(dev_no == bKEY_DOWN)
    {
        bMenuAction(MENU_DOWN);
    }
    else if(dev_no == bKEY_ENTER)
    {
        bMenuAction(MENU_ENTER);
    }
    else if(dev_no == bKEY_BACK)
    {
        bMenuAction(MENU_BACK);
    }
}
/********************************************************************************/
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main()
{
    BoardInit();

    SysTick_Config(SystemCoreClock / TICK_FRQ_HZ);
    NVIC_SetPriority(SysTick_IRQn, 0x0);

    bInit();
    bShellInit();
    bGUI_ADD_DEVICE(bTFT, NULL, 240, 320, TOUCH_TYPE_RES);
    UG_FillScreen(C_BLACK);
    
    bBUTTON_ADD_KEY(bKEY_UP, BTN_EVENT_CLICK, BtnEventHandler);
    bBUTTON_ADD_KEY(bKEY_DOWN, BTN_EVENT_CLICK, BtnEventHandler);
    bBUTTON_ADD_KEY(bKEY_ENTER, BTN_EVENT_CLICK, BtnEventHandler);
    bBUTTON_ADD_KEY(bKEY_BACK, BTN_EVENT_CLICK, BtnEventHandler);
    
    bMenuInit();
    
    while (1)
    {
        bExec();
    }
}

void SysTick_Handler()
{
    bHalIncSysTick();
}

void USART1_IRQHandler()
{
    uint8_t uart_dat = 0;
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        uart_dat = USART_ReceiveData(USART1);
        bShellParse(&uart_dat, 1);
    }
}

#ifdef USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
