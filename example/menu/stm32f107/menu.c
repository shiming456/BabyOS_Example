#include "menu.h"
#include "b_os.h"

#define LEVEL0_MENU0_ID (0x00)
#define LEVEL0_MENU1_ID (0x01)
#define LEVEL0_MENU2_ID (0x02)

#define LEVEL1_MENU0_ID (0x10)
#define LEVEL1_MENU1_ID (0x11)
#define LEVEL1_MENU2_ID (0x12)



void Level0Menu0F(uint32_t pre_id)
{
    if(pre_id == LEVEL0_MENU1_ID || pre_id == LEVEL0_MENU2_ID)
    {
        UG_PutString(10, 40, "->");
        UG_PutString(10, 80, "  ");
        UG_PutString(10, 120, "  ");
    }
    else
    {
        UG_FillScreen(C_BLACK);
        UG_PutString(10, 40, "-> project name");
        UG_PutString(10, 80, "   fw version");
        UG_PutString(10, 120, "   hw version"); 
    }
}

void Level0Menu1F(uint32_t pre_id)
{
    if(pre_id == LEVEL0_MENU0_ID || pre_id == LEVEL0_MENU2_ID)
    {
        UG_PutString(10, 40, "  ");
        UG_PutString(10, 80, "->");
        UG_PutString(10, 120, "  ");
    }
    else
    {
        UG_FillScreen(C_BLACK);
        UG_PutString(10, 40, "   project name");
        UG_PutString(10, 80, "-> fw version");
        UG_PutString(10, 120, "   hw version"); 
    }
}

void Level0Menu2F(uint32_t pre_id)
{
    if(pre_id == LEVEL0_MENU0_ID || pre_id == LEVEL0_MENU1_ID)
    {
        UG_PutString(10, 40, "  ");
        UG_PutString(10, 80, "  ");
        UG_PutString(10, 120, "->");
    }
    else
    {
        UG_FillScreen(C_BLACK);
        UG_PutString(10, 40, "   project name");
        UG_PutString(10, 80, "   fw version");
        UG_PutString(10, 120, "-> hw version"); 
    }
}

void Level1Menu0F(uint32_t pre_id)
{
    UG_FillScreen(C_BLACK);
    UG_PutString(10, 40, "babyos");
}

void Level1Menu1F(uint32_t pre_id)
{
    char buf[64];
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "V%d.%d.%d", FW_VERSION / 10000, (FW_VERSION % 10000) / 100, FW_VERSION % 100 );
    UG_FillScreen(C_BLACK);
    UG_PutString(10, 40, buf);
}

void Level1Menu2F(uint32_t pre_id)
{
    char buf[64];
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "V%d.%d.%d", HW_VERSION / 10000, (HW_VERSION % 10000) / 100, HW_VERSION % 100 );    
    UG_FillScreen(C_BLACK);
    UG_PutString(10, 40, buf);
}

int bMenuInit()
{
    bMenuAddSibling(LEVEL0_MENU0_ID, LEVEL0_MENU0_ID, Level0Menu0F);
    bMenuAddSibling(LEVEL0_MENU0_ID, LEVEL0_MENU1_ID, Level0Menu1F);
    bMenuAddSibling(LEVEL0_MENU1_ID, LEVEL0_MENU2_ID, Level0Menu2F);
    
    bMenuAddChild(LEVEL0_MENU0_ID, LEVEL1_MENU0_ID, Level1Menu0F);
    bMenuAddChild(LEVEL0_MENU1_ID, LEVEL1_MENU1_ID, Level1Menu1F);
    bMenuAddChild(LEVEL0_MENU2_ID, LEVEL1_MENU2_ID, Level1Menu2F);
    
    return 0;
}










