/**
 * \file offline.c
 * \brief
 * \version 0.1
 * \date 2022-10-22
 * \author notrynohigh (notrynohigh@outlook.com)
 *
 * Copyright (c) 2020 by notrynohigh. All Rights Reserved.
 */
#include "b_os.h"
#include "state.h"

/*
    离线状态，每隔10秒ping一次www.baidu.com判断是否联网
    如果联网则切换至online状态
*/

#define CFG_STA_MODE (0)
#define PING_BAIDU (1)

static uint32_t s_Check    = 0;
static uint32_t s_Tick     = 0;
static uint32_t s_PingFlag = 0;

static void _EnterOffline(uint32_t pre_state)
{
    b_log_i("state %d --> offline(%d) \n", pre_state, STATE_OFFLINE);
    s_Check    = 0;
    s_Tick     = bHalGetSysTick();
    s_PingFlag = CFG_STA_MODE;
}

static void _PingFunc()
{
    int fd = -1;
    fd     = bOpen(bESP12F, BCORE_FLAG_RW);
    if (fd < 0)
    {
        b_log_e("open failed...\n");
        return;
    }
    switch (s_PingFlag)
    {
        case CFG_STA_MODE:
        {
            bCtl(fd, bCMD_WIFI_MODE_STA, NULL);
        }
        break;
        case PING_BAIDU:
        {
            bCtl(fd, bCMD_WIFI_PING, "www.baidu.com");
        }
        break;
        default:
            break;
    }
    bClose(fd);
}

static void _OfflineHandler()
{
    bDeviceMsg_t msg;
    if (s_Check == 0)
    {
        if (bHalGetSysTick() - s_Tick >= MS2TICKS(1000))
        {
            s_Tick = bHalGetSysTick();
            _PingFunc();
            s_Check = 1;
        }
    }
    else
    {
        bDeviceReadMessage(bESP12F, &msg);
        if (((bEsp12fPrivate_t *)msg._p)->result != ESP12F_CMD_RESULT_NULL || 
             (bHalGetSysTick() - s_Tick >= MS2TICKS(10000)))
        {
            s_Tick  = bHalGetSysTick();
            s_Check = 0;
        }
        if (((bEsp12fPrivate_t *)msg._p)->result == ESP12F_CMD_RESULT_OK)
        {
            if (s_PingFlag != PING_BAIDU)
            {
                s_PingFlag += 1;
            }
            else
            {
                bStateTransfer(STATE_ONLINE);
            }
        }
    }
}

static void _EventHandler(uint32_t event, void *arg)
{
    if (event == EVENT_CFG_NET)
    {
        bStateTransfer(STATE_NETCFG);
    }
}

//----------------------------------------------------------------------------------------------

static const bStateEvent_t s_OfflineEventTable[] = {
    {EVENT_CFG_NET, _EventHandler},
};

static const bStateInfo_t s_OfflineStateInfo = {
    .state   = STATE_OFFLINE,
    .enter   = _EnterOffline,
    .exit    = NULL,
    .handler = _OfflineHandler,
    .event_table =
        {
            .p_event_table = (bStateEvent_t *)&s_OfflineEventTable[0],
            .number        = (sizeof(s_OfflineEventTable) / sizeof(bStateEvent_t)),
        },
};

bSTATE_REG_INSTANCE(s_OfflineStateInfo);
