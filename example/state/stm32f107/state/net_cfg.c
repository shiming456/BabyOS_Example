/**
 * \file net_cfg.c
 * \brief
 * \version 0.1
 * \date 2022-10-22
 * \author notrynohigh (notrynohigh@outlook.com)
 *
 * Copyright (c) 2020 by notrynohigh. All Rights Reserved.
 */
#include <stdio.h>

#include "b_os.h"
#include "state.h"

/*
    配网状态，切换为AP模式
*/

#define CFG_AP_MODE (0)
#define CFG_TCP_SERVER (1)
#define CFG_WAIT_DATA (2)
#define CFG_JOIN_AP (3)

static uint32_t s_Check   = 0;
static uint32_t s_Tick    = 0;
static uint32_t s_CfgFlag = 0;

const bWifiApInfo_t ApInfo = {
    .ssid       = "babyos_wifi",
    .encryption = 0,
};

bWifiApInfo_t ApRoutInfo;

const bTcpUdpInfo_t TcpServerInfo = {
    .port = 666,
};

static void _EnterNetCfg(uint32_t pre_state)
{
    b_log_i("state %d --> net_cfg(%d) \n", pre_state, STATE_NETCFG);
    s_Check   = 0;
    s_Tick    = bHalGetSysTick();
    s_CfgFlag = CFG_AP_MODE;
    memset(&ApRoutInfo, 0, sizeof(ApRoutInfo));
}

static void _NetCfgFunc()
{
    int fd = -1;
    fd     = bOpen(bESP12F, BCORE_FLAG_RW);
    if (fd < 0)
    {
        b_log_e("open failed...\n");
        return;
    }
    switch (s_CfgFlag)
    {
        case CFG_AP_MODE:
        {
            bCtl(fd, bCMD_WIFI_MODE_STA_AP, (void *)&ApInfo);
        }
        break;
        case CFG_TCP_SERVER:
        {
            bCtl(fd, bCMD_WIFI_LOCAL_TCP_SERVER, (void *)&TcpServerInfo);
        }
        break;
        case CFG_JOIN_AP:
        {
            bCtl(fd, bCMD_WIFI_JOIN_AP, (void *)&ApRoutInfo);
        }
        break;
        default:
            break;
    }
    bClose(fd);
}

static void _NetCfgHandler()
{
    int          ret = -1;
    int          fd  = -1;
    bDeviceMsg_t msg;
    bWiFiData_t  RecData;
    if (s_Check == 0)
    {
        if (bHalGetSysTick() - s_Tick >= MS2TICKS(1000))
        {
            s_Tick = bHalGetSysTick();
            _NetCfgFunc();
            s_Check = 1;
        }
    }
    else
    {
        bDeviceReadMessage(bESP12F, &msg);
        if (((bEsp12fPrivate_t *)msg._p)->result != ESP12F_CMD_RESULT_NULL || 
            (bHalGetSysTick() - s_Tick >= MS2TICKS(10000)))
        {
            s_Tick  = bHalGetSysTick();
            s_Check = 0;
        }
        if (((bEsp12fPrivate_t *)msg._p)->result == ESP12F_CMD_RESULT_OK)
        {
            if (s_CfgFlag < CFG_WAIT_DATA)
            {
                s_CfgFlag += 1;
            }
            else if (s_CfgFlag == CFG_JOIN_AP)
            {
                bStateTransfer(STATE_OFFLINE);
            }
        }
    }

    if (s_CfgFlag >= CFG_WAIT_DATA)
    {
        fd = bOpen(bESP12F, BCORE_FLAG_RW);
        if (fd < 0)
        {
            b_log_e("open failed...\n");
            return;
        }

        //读取数据，来自TCP或者MQTT订阅主题
        if (bRead(fd, (uint8_t *)&RecData, sizeof(bWiFiData_t)) > 0)
        {
            if (RecData.mqtt.pstr != NULL)
            {
                //打印主题和收到的消息
                b_log("%s, %s\n", RecData.mqtt.topic.topic, RecData.mqtt.pstr);
            }
            if (RecData.tcp.pstr != NULL)
            {
                b_log("tcp:%s\n", RecData.tcp.pstr);
                ret = sscanf(RecData.tcp.pstr, "s:%s p:%s", ApRoutInfo.ssid, ApRoutInfo.passwd);
                if (ret == 2)
                {
                    b_log("\nssid:%s\npasswd:%s\n", ApRoutInfo.ssid, ApRoutInfo.passwd);
                    if (strlen(ApRoutInfo.passwd) > 0)
                    {
                        ApRoutInfo.encryption = 2;
                        s_CfgFlag             = CFG_JOIN_AP;
                        s_Check               = 0;
                    }
                }
            }
            //释放空间
            WIFI_DATA_USE_END(RecData);
        }
        bClose(fd);
    }
}

static void _EventHandler(uint32_t event, void *arg)
{
    if (event == EVENT_CFG_NET)
    {
        bStateTransfer(STATE_OFFLINE);
    }
}

//----------------------------------------------------------------------------------------------

static const bStateEvent_t s_NetCfgEventTable[] = {
    {EVENT_CFG_NET, _EventHandler},
};

static const bStateInfo_t s_NetCfgStateInfo = {
    .state   = STATE_NETCFG,
    .enter   = _EnterNetCfg,
    .exit    = NULL,
    .handler = _NetCfgHandler,
    .event_table =
        {
            .p_event_table = (bStateEvent_t *)&s_NetCfgEventTable[0],
            .number        = (sizeof(s_NetCfgEventTable) / sizeof(bStateEvent_t)),
        },
};

bSTATE_REG_INSTANCE(s_NetCfgStateInfo);
