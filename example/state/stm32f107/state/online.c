/**
 * \file online.c
 * \brief
 * \version 0.1
 * \date 2022-10-22
 * \author notrynohigh (notrynohigh@outlook.com)
 *
 * Copyright (c) 2020 by notrynohigh. All Rights Reserved.
 */
#include "b_os.h"
#include "state.h"
/*
    在线状态，
    配置MQTT信息，每隔1分钟发布一条消息
    收到数据则打印
*/

#define MQTT_CONN (0)
#define MQTT_SUB (1)
#define MQTT_PUB (2)

static uint32_t s_Check = 0;
static uint32_t s_Tick  = 0;
// flag 0:配置MQTT信息 1：订阅主题 2:发布消息
static uint32_t s_MqttFlag = 0;

static const bMqttConnInfo_t MqttInfo = {
    .broker    = "babyos.cn",
    .port      = 1883,
    .device_id = "esp12f",
    .user      = "babyos",
    .passwd    = "babyos",
};

static const bMqttTopicInfo_t TopicInfo = {
    .topic = "baby",
    .qos   = 1,
};

static bMqttTopicData_t TopicData = {
    .topic.topic = "os",
    .topic.qos   = 1,
    .pstr        = NULL,
};

static void _EnterOnline(uint32_t pre_state)
{
    b_log_i("state %d --> Online(%d) \n", pre_state, STATE_ONLINE);
    s_Check        = 0;
    s_MqttFlag     = MQTT_CONN;
    s_Tick         = bHalGetSysTick();
    TopicData.pstr = bMalloc(128);
    if (TopicData.pstr == NULL)
    {
        b_log_e("malloc error...\n");
    }
    memcpy(TopicData.pstr, "hello babyos", strlen("hello babyos"));
}

static void _ExitOnline()
{
    if (TopicData.pstr)
    {
        bFree(TopicData.pstr);
        TopicData.pstr = NULL;
    }
}

static void _MqttFunc()
{
    int fd = -1;
    fd     = bOpen(bESP12F, BCORE_FLAG_RW);
    if (fd < 0)
    {
        b_log_e("open failed...\n");
        return;
    }
    switch (s_MqttFlag)
    {
        case MQTT_CONN:
        {
            bCtl(fd, bCMD_WIFI_MQTT_CONN, (void *)&MqttInfo);
        }
        break;
        case MQTT_SUB:
        {
            bCtl(fd, bCMD_WIFI_MQTT_SUB, (void *)&TopicInfo);
        }
        break;
        case MQTT_PUB:
        {
            bCtl(fd, bCMD_WIFI_MQTT_PUB, &TopicData);
        }
        break;
        default:
            break;
    }
    bClose(fd);
}

static void _OnlineHandler()
{
    int            fd = -1;
    bWiFiData_t    RecData;
    bDeviceMsg_t   msg;
    static uint8_t err_count = 0;
    if (s_Check == 0)
    {
        if ((bHalGetSysTick() - s_Tick >= MS2TICKS(1000) && (s_MqttFlag < MQTT_PUB)) ||
            (bHalGetSysTick() - s_Tick >= MS2TICKS(60000) && (s_MqttFlag == MQTT_PUB)))
        {
            s_Tick = bHalGetSysTick();
            _MqttFunc();
            s_Check = 1;
        }
    }
    else
    {
        bDeviceReadMessage(bESP12F, &msg);
        if (((bEsp12fPrivate_t *)msg._p)->result != ESP12F_CMD_RESULT_NULL || 
            (bHalGetSysTick() - s_Tick >= MS2TICKS(10000)))
        {
            s_Tick = bHalGetSysTick();
            b_log("result %d \n", ((bEsp12fPrivate_t *)msg._p)->result);
            s_Check = 0;
        }
        if (((bEsp12fPrivate_t *)msg._p)->result == ESP12F_CMD_RESULT_OK)
        {
            if (s_MqttFlag != MQTT_PUB)
            {
                s_MqttFlag += 1;
            }
            err_count = 0;
        }
        else if (((bEsp12fPrivate_t *)msg._p)->result == ESP12F_CMD_RESULT_ERR)
        {
            b_log("error %d \n", s_MqttFlag);
            err_count += 1;
            if (err_count > 3)
            {
                err_count = 0;
                bStateTransfer(STATE_OFFLINE);
            }
        }
    }

    fd = bOpen(bESP12F, BCORE_FLAG_RW);
    if (fd < 0)
    {
        b_log_e("open failed...\n");
        return;
    }
    //读取数据，来自TCP或者MQTT订阅主题
    if (bRead(fd, (uint8_t *)&RecData, sizeof(bWiFiData_t)) > 0)
    {
        if (RecData.mqtt.pstr != NULL)
        {
            //打印主题和收到的消息
            b_log("%s, %s\n", RecData.mqtt.topic.topic, RecData.mqtt.pstr);
        }
        if (RecData.tcp.pstr != NULL)
        {
            b_log("tcp:%s\n", RecData.tcp.pstr);
        }
        //释放空间
        WIFI_DATA_USE_END(RecData);
    }
    bClose(fd);
}

static void _EventHandler(uint32_t event, void *arg)
{
    if (event == EVENT_CFG_NET)
    {
        bStateTransfer(STATE_NETCFG);
    }
}

//----------------------------------------------------------------------------------------------

static const bStateEvent_t s_OnlineEventTable[] = {
    {EVENT_CFG_NET, _EventHandler},
};

static const bStateInfo_t s_OnlineStateInfo = {
    .state   = STATE_ONLINE,
    .enter   = _EnterOnline,
    .exit    = _ExitOnline,
    .handler = _OnlineHandler,
    .event_table =
        {
            .p_event_table = (bStateEvent_t *)&s_OnlineEventTable[0],
            .number        = (sizeof(s_OnlineEventTable) / sizeof(bStateEvent_t)),
        },
};

bSTATE_REG_INSTANCE(s_OnlineStateInfo);
