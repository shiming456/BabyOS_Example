/**
 * \file state.h
 * \brief
 * \version 0.1
 * \date 2022-10-22
 * \author notrynohigh (notrynohigh@outlook.com)
 *
 * Copyright (c) 2020 by notrynohigh. All Rights Reserved.
 */

#ifndef __STATE_H__
#define __STATE_H__

#define STATE_OFFLINE (0)
#define STATE_ONLINE (1)
#define STATE_NETCFG (2)

#define EVENT_CFG_NET (0)

#endif
