/**
 ******************************************************************************
 * @file    Project/STM32F10x_StdPeriph_Template/main.c
 * @author  MCD Application Team
 * @version V3.3.0
 * @date    04/16/2010
 * @brief   Main program body
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "b_os.h"
#include "board.h"

/** @addtogroup Template_Project
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

void _StartAdc()
{
    ADC_SoftwareStartInjectedConvCmd(ADC1, ENABLE);
}

void _AdcCallback(uint32_t ad_val, uint32_t arg)
{
    uint32_t Temp = 0;
    b_log("%d:%d\r\n", arg, ad_val);
    if (arg == 2)
    {
        Temp = (1.43 - ad_val * 3.3 / 4096) * 1000 / 4.35 + 25;
        b_log("T:%d\r\n", Temp);
    }
}

//此处定义实例，序号分别填的是10和16，在喂数据时候要对应
//由于使用同一个回调函数，那么回调带入的参数要区分，分别是1 和 2
bADC_INSTANCE(ADTest, 10, 1, _AdcCallback, 1);
bADC_INSTANCE(ADTemp, 16, 1, _AdcCallback, 2);

/********************************************************************************/
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main()
{
    BoardInit();

    SysTick_Config(SystemCoreClock / TICK_FRQ_HZ);
    NVIC_SetPriority(SysTick_IRQn, 0x0);

    bInit();
    bAdchubRegist(&ADTest);
    bAdchubRegist(&ADTemp);
    while (1)
    {
        bExec();
        BOS_PERIODIC_TASK(_StartAdc, 500);
    }
}

void SysTick_Handler()
{
    bHalIncSysTick();
}

void USART1_IRQHandler()
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        USART_ReceiveData(USART1);
    }
}

void ADC1_2_IRQHandler()
{
    uint32_t tmp = 0;
    if (ADC_GetITStatus(ADC1, ADC_IT_JEOC) == SET)
    {
        ADC_ClearITPendingBit(ADC1, ADC_IT_JEOC);

        tmp = ADC_GetInjectedConversionValue(ADC1, ADC_InjectedChannel_1);
        bAdchubFeedValue(10, tmp);
        tmp = ADC_GetInjectedConversionValue(ADC1, ADC_InjectedChannel_2);
        bAdchubFeedValue(16, tmp);
    }
}

#ifdef USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
