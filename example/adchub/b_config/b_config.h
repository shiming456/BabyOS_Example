#ifndef __B_CONFIG_H__ 
#define __B_CONFIG_H__ 


#define HW_VERSION 211212
#define FW_VERSION 80106
#define FW_NAME "BabyOS"
#define TICK_FRQ_HZ 1000
#define VENDOR_ST 1
#define STM32F10X_CL 1
#define _BOS_ALGO_ENABLE 1
#define PCF8574_DEFAULT_OUTPUT 0
#define MATRIX_KEYS_ROWS 4
#define MATRIX_KEYS_COLUMNS 4
#define _BOS_MODULES_ENABLE 1
#define _ADCHUB_ENABLE 1
#define FILTER_BUF_SIZE 10
#define _DEBUG_ENABLE 1
#define LOG_UART 0
#define LOG_LEVEL_INFO 1
#define LOG_BUF_SIZE 256


#include "b_type.h" 

#endif 

